#!/usr/bin/python3

from sys import argv
from importlib import import_module
import os

SEUIL_AFFICHAGE = 2

SUCCES = 0
ECHEC = 1
ERREUR = 2

STATUTS = ["Succès", "Échec", "Erreur"]

NOMS_NIVEAUX = ["TOTAL", "TESTS DE BASE", "TESTS AVANCÉS", "TESTS SECONDAIRES"]
NOM_NIVEAU_GENERIQUE = "TESTS DE NIVEAU "

SEPARATEUR = "=" * 80
SEPARATEUR_INTERMEDIAIRE = "-" * 80

def get_emoji(correct):
    if correct:
        return "😀"
    else:
        return "😟"
    
def importer_module(nom_module):
    nom_module = nom_module.replace("/", ".")
    module = import_module(nom_module)
    for nom_element in dir(module):
        element = getattr(module, nom_element)
        globals()[nom_element] = element

class Stat:
    def __init__(self, nbTests, nbEchecs, nbErreurs):
        self.nbTests = nbTests
        self.nbEchecs = nbEchecs
        self.nbErreurs = nbErreurs
        
class Totalisateur:
    def __init__(self):
        self.nbTests = 0
        self.nbEchecs = 0
        self.nbErreurs = 0

    def ajouterStat(self, stat):
        self.nbTests += stat.nbTests
        self.nbEchecs += stat.nbEchecs
        self.nbErreurs += stat.nbErreurs

    def estCorrect(self):
        return self.nbEchecs + self.nbErreurs == 0

    def __str__(self):
        retour = f"  Nombre de tests : {self.nbTests}, Nombre d'échecs : {self.nbEchecs}, Nombre d'erreurs : {self.nbErreurs}\n"
        return retour

    def toLigneTableau(self, niveau, largeur_colonne_titre, largeur_colonnes_stats, largeur_colonne_statut):
        statut = get_emoji(self.estCorrect())
        nom_niveau = get_nom_niveau(niveau)
        return f"|{nom_niveau:{largeur_colonne_titre}}|{self.nbTests:{largeur_colonnes_stats}}|{self.nbEchecs:{largeur_colonnes_stats}}|{self.nbErreurs:{largeur_colonnes_stats}}|{statut:^{largeur_colonne_statut-1}}|"


class Resultat:
    def __init__(self, message, code, statut):
        self.message = message
        self.code = code
        self.statut = statut
        
class Rapport:
    def __init__(self, titre, niveau):
        self.titre = titre
        self.niveau = niveau
        self.resultats = [[],[],[]]
        self.vide = True
        self.codesErrones = []
        
    def ajouterResultatTest(self, resultat):
        self.vide = False
        self.resultats[resultat.statut].append(resultat.message)
        if resultat.statut != SUCCES:
            self.codesErrones.append(resultat.code)
        
    def estVide(self):
        return self.vide

    def estSucces(self):
        return len(self.resultats[ECHEC]) + len(self.resultats[ERREUR]) == 0

    def longueurMessageMax(self):
        if len(self.resultats[ECHEC]) == 0:
            l1 = 0
        else:
            l1 = max(map(len, self.resultats[ECHEC]))
        if len(self.resultats[ERREUR]) == 0:
            l2 = 0
        else:
            l2 = max(map(len, self.resultats[ERREUR]))
        return max(l1, l2)
    
    def stats(self):
        nbSucces = len(self.resultats[SUCCES])
        nbEchecs = len(self.resultats[ECHEC])
        nbErreurs = len(self.resultats[ERREUR])
        nbTests = nbSucces + nbEchecs + nbErreurs
        return Stat(nbTests, nbEchecs, nbErreurs)
    
    def strStats(self):
        stat = self.stats()
        retour = f"  Nombre de tests : {stat.nbTests}, Nombre d'échecs : {stat.nbEchecs}, Nombre d'erreurs : {stat.nbErreurs}\n"
        return retour
        
    def __str__(self):
        retour = self.titre + " "
        retour += get_emoji(self.estSucces()) + "\n"
        if not self.estSucces():
            retour += self.strStats()
            if len(self.resultats[ECHEC]) > 0:
                retour += "  Tests ayant échoué :\n"
                for message in self.resultats[ECHEC]:
                    retour +=  "    " + message + "\n"
            if len(self.resultats[ERREUR]) > 0:
                retour += "  Tests ayant mené à une erreur :\n"
                for message in self.resultats[ERREUR]:
                    retour += "    " + message + "\n"
        return retour

    def toLigneTableau(self, largeur_colonne_titre, largeur_colonnes_stats, largeur_colonne_statut):
        stat = self.stats()
        statut = get_emoji(self.estSucces())
        return f"|{self.titre:{largeur_colonne_titre}}|{stat.nbTests:{largeur_colonnes_stats}}|{stat.nbEchecs:{largeur_colonnes_stats}}|{stat.nbErreurs:{largeur_colonnes_stats}}|{statut:^{largeur_colonne_statut-1}}|"

    def detailsToTableau(self, largeur_colonne_titre, largeur_colonne_statut):
        retour = ""
        for cas in self.resultats[ECHEC]:
            ligne = f"|{cas:{largeur_colonne_titre}}|{STATUTS[ECHEC]:{largeur_colonne_statut}}|\n"
            retour += ligne
        for cas in self.resultats[ERREUR]:
            ligne = f"|{cas:{largeur_colonne_titre}}|{STATUTS[ERREUR]:{largeur_colonne_statut}}|\n"
            retour += ligne
        return retour
        
def executer_un_test(le_test):
    try:
        exec(le_test)
        resultat = Resultat("OK", le_test, SUCCES)
    except AssertionError as ae:
        resultat = Resultat(str(ae), le_test, ECHEC)
    except Exception as ae:
        resultat = Resultat(str(ae), le_test, ERREUR)
    return resultat

def degre_specificite(descriptif):
    degre = 0
    i = 0
    for caractere in descriptif:
        if caractere == "#":
            degre += 1
        else:
            break
    return degre

def executer_les_tests(liste_des_cas):
    liste_rapports = []
    rapport = Rapport("Rapport par défaut", 1)
    liste_cas = liste_des_cas.split("\n")
    mode_bloc = False
    bloc = ""
    for cas in liste_cas:
        if mode_bloc:
            if cas == "END":
                cas = bloc
                mode_bloc = False
            else:
                bloc += "\n" + cas
                continue
        if cas == "BEGIN":
            mode_bloc = True
            bloc = ""
        elif cas == "":
            continue
        elif cas.startswith("#"):
            if not rapport.estVide():
                liste_rapports.append(rapport)
            niveau = degre_specificite(cas)
            titre = cas[niveau:]
            rapport = Rapport(titre, niveau)
        else:
            resultat = executer_un_test(cas)
            rapport.ajouterResultatTest(resultat)
    if not rapport.estVide():
        liste_rapports.append(rapport)
    return liste_rapports

def get_nom_niveau(niveau):
    if niveau < len(NOMS_NIVEAUX):
        nom_niveau = NOMS_NIVEAUX[niveau]
    else:
        nom_niveau = NOM_NIVEAU_GENERIQUE + str(niveau)
    return nom_niveau

def afficher_rapport_verbeux(rapports):
    rapports.sort(key=lambda r: r.niveau)
    niveau_courant = -1
    totalisateur_general = Totalisateur()
    totalisateur_niveau = None
    for rapport in rapports:
        if rapport.niveau != niveau_courant:
            if totalisateur_niveau is not None:
                print(SEPARATEUR_INTERMEDIAIRE)
                print("BILAN POUR LES", get_nom_niveau(niveau_courant), get_emoji(totalisateur_niveau.estCorrect()))
                print(totalisateur_niveau)
            niveau_courant = rapport.niveau
            print(SEPARATEUR)
            print(get_nom_niveau(niveau_courant))
            print(SEPARATEUR)
            totalisateur_niveau = Totalisateur()
        print(rapport)
        stats = rapport.stats()
        totalisateur_niveau.ajouterStat(stats)
        totalisateur_general.ajouterStat(stats)
    print(SEPARATEUR_INTERMEDIAIRE)
    print("BILAN POUR LES", get_nom_niveau(niveau_courant), get_emoji(totalisateur_niveau.estCorrect()))
    print(totalisateur_niveau)
    print(SEPARATEUR)
    print("BILAN GÉNÉRAL", get_emoji(totalisateur_niveau.estCorrect()))
    print(totalisateur_general)
    return totalisateur_general.estCorrect()

def afficher_rapport_tableau_niveaux(rapports_tries):
    longueur_categorie_max_1 = max(map(len, NOMS_NIVEAUX))
    longueur_categorie_max_2 = len(NOM_NIVEAU_GENERIQUE) + 3
    largeur_colonne_titre = max(longueur_categorie_max_1, longueur_categorie_max_2)
    titre_colonne_1 = "Niveaux de test"
    titres_colonnes_stats = ["Nb tests", "Nb échecs", "Nb erreurs"]
    largeur_colonnes_stats = max(map(len, titres_colonnes_stats))
    titre_statut = "Statut"
    largeur_colonne_statut = len(titre_statut)
    separateur = "|" + "=" * (largeur_colonne_titre + 3 * largeur_colonnes_stats + largeur_colonne_statut+4) + "|"
    separateur_intermediaire = "|" + "-" * (largeur_colonne_titre + 3 * largeur_colonnes_stats + largeur_colonne_statut+4) + "|"
    print(separateur)
    print(f"|{titre_colonne_1:^{largeur_colonne_titre}}|{titres_colonnes_stats[0]:^{largeur_colonnes_stats}}|{titres_colonnes_stats[1]:^{largeur_colonnes_stats}}|{titres_colonnes_stats[2]:^{largeur_colonnes_stats}}|{titre_statut:^{largeur_colonne_statut}}|")
    print(separateur)
    niveau_courant = -1
    debut = True
    totalisateur_general = Totalisateur()
    totalisateur_niveau = Totalisateur()
    for rapport in rapports:
        stats = rapport.stats()
        totalisateur_niveau.ajouterStat(stats)
        totalisateur_general.ajouterStat(stats)
        if rapport.niveau != niveau_courant:
            if debut:
                debut = False
            else:
                print(separateur_intermediaire)
            niveau_courant = rapport.niveau
            print(totalisateur_niveau.toLigneTableau(niveau_courant, largeur_colonne_titre, largeur_colonnes_stats, largeur_colonne_statut))
            totalisateur_niveau = Totalisateur()
    print(separateur)
    print(totalisateur_general.toLigneTableau(0, largeur_colonne_titre, largeur_colonnes_stats, largeur_colonne_statut))
    print(separateur)
    return totalisateur_general.estCorrect()

def afficher_rapport_tableau_jeux(rapports_tries):
    largeur_colonne_titre = max(map(lambda rapport: len(rapport.titre), rapports))
    titre_colonne_1 = "Jeux de tests"
    titres_colonnes_stats = ["Nb tests", "Nb échecs", "Nb erreurs"]
    largeur_colonnes_stats = max(map(len, titres_colonnes_stats))
    titre_statut = "Statut"
    largeur_colonne_statut = len(titre_statut)
    separateur = "|" + "=" * (largeur_colonne_titre + 3 * largeur_colonnes_stats + largeur_colonne_statut+4) + "|"
    separateur_intermediaire = "|" + "-" * (largeur_colonne_titre + 3 * largeur_colonnes_stats + largeur_colonne_statut+4) + "|"
    print(separateur)
    print(f"|{titre_colonne_1:^{largeur_colonne_titre}}|{titres_colonnes_stats[0]:^{largeur_colonnes_stats}}|{titres_colonnes_stats[1]:^{largeur_colonnes_stats}}|{titres_colonnes_stats[2]:^{largeur_colonnes_stats}}|{titre_statut:^{largeur_colonne_statut}}|")
    print(separateur)
    niveau_courant = -1
    totalisateur_general = Totalisateur()
    totalisateur_niveau = None
    debut = True
    for rapport in rapports:
        if rapport.niveau != niveau_courant:
            if debut:
                debut = False
            else:
                print(separateur_intermediaire)
            niveau_courant = rapport.niveau
        print(rapport.toLigneTableau(largeur_colonne_titre, largeur_colonnes_stats, largeur_colonne_statut))
    print(separateur)

def afficher_rapport_tableau_cas(rapports_tries):
    largeur_colonne_titre = max(map(lambda rapport: rapport.longueurMessageMax(), rapports))
    titre_colonne_1 = "Cas de test"
    titre_statut = "Statut"
    largeur_colonne_statut = len(titre_statut) + 2
    separateur = "|" + "=" * (largeur_colonne_titre + largeur_colonne_statut + 1) + "|"
    separateur_intermediaire = "|" + "-" * (largeur_colonne_titre + largeur_colonne_statut + 1) + "|"
    print(separateur)
    print(f"|{titre_colonne_1:^{largeur_colonne_titre}}|{titre_statut:^{largeur_colonne_statut}}|")
    print(separateur)
    debut = True
    niveau_courant = -1
    for rapport in rapports:
        if rapport.niveau != niveau_courant:
            if debut:
                debut = False
            else:
                print(separateur_intermediaire)
            niveau_courant = rapport.niveau
        print(rapport.detailsToTableau(largeur_colonne_titre, largeur_colonne_statut), end='')
    print(separateur)
    
def afficher_rapport_tableaux(rapports):
    rapports.sort(key=lambda r: r.niveau)
    correction = afficher_rapport_tableau_niveaux(rapports)
    print()
    afficher_rapport_tableau_jeux(rapports)
    print()
    afficher_rapport_tableau_cas(rapports)
    return correction

def afficher_rapport_resume(rapports):
    totalisateur_general = Totalisateur()
    totalisateur_base = Totalisateur()
    codes_errones = []
    codes_base_errones = []
    for rapport in rapports:
        stats = rapport.stats()
        totalisateur_general.ajouterStat(stats)
        if rapport.niveau == 1:
            totalisateur_base.ajouterStat(stats)
            codes_base_errones.extend(rapport.codesErrones)
        codes_errones.extend(rapport.codesErrones)
    if totalisateur_general.estCorrect():
        print(f"Bravo, {get_emoji(True)}, vous avez réussi tous les tests")
    elif not totalisateur_base.estCorrect():
        print("Les tests du sujet ne sont pas validés. Par exemple, le test suivant échoue :")
        print(codes_base_errones[0])
    elif totalisateur_general.nbEchecs + totalisateur_general.nbErreurs <= SEUIL_AFFICHAGE:
        print("C'est bien, vous avez réussi les tests proposés dans le sujet. Cependant, le test suivant a échoué :")
        print(codes_errones[0])
        if len(codes_errones) > 1:
            print("Nombre d'autres tests qui ne passent pas :", len(codes_errones))
    else:
        print("C'est assez bien, vous avez réussi les tests proposés dans le sujet. Cependant, de nombreux tests internes ont échoué, par exemple :")
        print(codes_errones[0])
        
    return totalisateur_general.estCorrect()
        
        
def correction_nom_fichier_a_tester(nom_fichier_a_tester):
    if nom_fichier_a_tester.endswith(".py"):
        correction = nom_fichier_a_tester[:-3]
    else:
        correction = nom_fichier_a_tester
    return correction

def correction_nom_fichier_tests(nom_fichier_tests):
    if not nom_fichier_tests.endswith(".py"):
        correction = nom_fichier_tests+ ".py"
    else:
        correction = nom_fichier_tests
    return correction

if __name__ == "__main__":
    if len(argv) < 3 or len(argv) > 5:
        print("Usage: python3 testeur.py fichier_à_tester.py jeu_de_test.py [--type_sortie=court|long|tableau] [--seuil=n]")
        exit(1)
    pwd = os.environ['PWD']
    os.environ['PYTHONPATH'] = pwd + ":" + os.environ['PYTHONPATH']
    nom_fichier_a_tester = argv[1]
    nom_fichier_a_tester = correction_nom_fichier_a_tester(nom_fichier_a_tester)
    importer_module(nom_fichier_a_tester)

    nom_fichier_tests = argv[2]
    nom_fichier_tests = correction_nom_fichier_tests(nom_fichier_tests)
    afficheur = afficher_rapport_resume
    for param in argv[3:]:
        if param.startswith("--type_sortie="):
            type_sortie = param[14:]
            print(type_sortie)
            if type_sortie == "court":
                afficheur = afficher_rapport_resume
            elif type_sortie == "long":
                afficheur = afficher_rapport_verbeux
            elif type_sortie == "tableau":
                afficheur = afficher_rapport_tableaux
            else:
                print("Type de sortie inconnu")
                exit(1)
        elif param.startswith("--seuil="):
            try:
                seuil_str = param[8:]
                print(seuil_str)
                SEUIL_AFFICHAGE = int(seuil_str)
                if SEUIL_AFFICHAGE < 1:
                    print("Seuil invalide")
                    exit(1)
            except:
                print("Seuil invalide")
                exit(1)
        else:
            print("Paramètre inconnu")
            exit(1)
    with open(nom_fichier_tests, 'r') as fichier_tests:
        tests = fichier_tests.read()
        rapports = executer_les_tests(tests)
        if afficheur(rapports):
            exit(0)
    exit(2)

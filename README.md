# Programme permettant de lancer tout un jeu de test sur un programme écrit en Python

Tous les tests sont exécutés, même si certains échouent ou plantent.

## Utilisation
`python3 testeur.py fichier_à_tester jeu_de_tests.py [--type-sortie=long|court|tableau --seuil=n]`

## Format du fichier avec le jeu de tests :
* Les lignes vides sont ignorées
* Les lignes commençant par un # permettent d'associer un titre à l'ensemble des cas de tests qui suivent. Ce titre sera affichier dans le rapport
* Les autres lignes doivent être des `assert` standards de Python, avec un message à chaque fois. Ces lignes peuvent éventuellement contenir plusieurs instructions pour par exemple initialiser des variables figurant dans le `assert`. En effet, les variables définies dans une ligne ne peuvent pas être utilisées à la ligne suivante.
* Il est possible de définir des cas de test sur plusieurs lignes. Pour cela, il faut mettre un "BEGIN" avant le bloc définissant un cas de test, et un "END" à la fin du bloc.
* Le nombre de *#* au début d'une ligne introduisant un jeu de tests correspond au niveau du test. 3 niveaux sont prédéfinis, mais il est possible d'utiliser plus de niveaux :
 * niveau 1 : Tests de base : ce qui est le "minimum vital" ;
 * niveau 2 : Tests avancés : les cas un peu plus compliqués (conditions aux limites par exemple) ;
 * niveau 3 : Tests secondaires : des situations un peu moins faciles à envisager (appel hors du domaine de définition par exemple) ;

## Rapport de sortie
Suivant le type de sortie, 3 rapports différents sont affichés :
* type **court** (type par défaut) : une phrase de résumé, affichant le détail de l'éventuel premier test qui ne passe pas
* type **verbeur** : Un rapport textuel est affiché, avec 2 parties :
    * Les différents titres des jeux de tests, avec la liste des cas de test qui ne sont pas passés (😀 si tous les cas de test ont réussi) ;
    * Un bilan chiffré (nombre total de cas de tests, nombre de cas de test en échec, nombre de cas de test en erreur) par jeu de tests, par niveau de test, et globalement.
* type **tableau** : 3 tableaux affichant :
    * un résumé par *niveau* de test ;
    * un résumé par jeu de tests ;
    * un détail des tests erronés
    
Enfin, un code de retour (0 si tous les cas de tests sont passés, 1 s'il y a eu des echecs ou des erreurs)

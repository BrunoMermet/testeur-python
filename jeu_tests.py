#Cas de base
assert f(1/0) == 0, "évalution pour 1"
assert f(0) == 1, "évaluation pour 0"

#Autre cas
assert f(5) == 5,  "cas récursif, pour 5"
assert f(8) == 21, "cas récursif, pour 8"
indice = 9; resultat = 34; assert f(indice) == resultat, "cas récursif pour 9"

#Encore un autre cas
BEGIN
a = 5
b = 6
assert f(a) == b, "cas en plusieurs lignes"
END

##Cas secondaire
assert f(-1) == None, "gestion des nombres négatifs"

###Troisième niveau de test, première partie
assert f(6) == 8, "fibo(6)"

###Troisième niveau de test, deuxième partie
assert f(7) == 13, "fibo(13)"
assert f(9) == 34, "fibo(9)"

####Quatrième niveau de test
assert f(20) == 20, "fibo(20)"

# Question 1 : 

assert ecrete_val(-15, -10, 10) == -10    #écrêté car -15 est trop petit (pas entre -10 et 10)
assert ecrete_val(-10, -10, 10) == -10
assert ecrete_val( -8, -10, 10) == -8
assert ecrete_val(  7, -10, 10) == 7
assert ecrete_val( 10, -10, 10) == 10
assert ecrete_val( 18, -10, 10) == 10     #écrêté car 18 est trop grand (pas entre -10 et 10)

assert ecrete_val(209, 210, 220) == 210   #écrêté car 209 est trop petit (pas entre 210 et 220)
assert ecrete_val(210, 210, 220) == 210
assert ecrete_val(215, 210, 220) == 215
assert ecrete_val(220, 210, 220) == 220
assert ecrete_val(221, 210, 220) == 220   #écrêté car 221 est trop grand (pas entre 210 et 220)


# Question 2 : 

tab = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];tab2 = ecrete(tab, 5, 10);assert tab2 == [5, 5, 5, 5, 5, 5, 6, 7, 8, 9, 10, 10, 10, 10], "cas 1"

tab = [-13, -4, 6, 5, 8, -3, -12, -3, 0, 6, 7]
ecrete(tab, -10, -5)
assert tab == [-10, -5, -5, -5, -5, -5, -10, -5, -5, -5, -5]

tab = [7, 8, 3, 9, 8, 7, 2, 4, 8, 9, 0, 1, 5, 8, 8, 8, 4, 5, 4, 5]
ecrete(tab, 0, 10)
assert tab == [7, 8, 3, 9, 8, 7, 2, 4, 8, 9, 0, 1, 5, 8, 8, 8, 4, 5, 4, 5]

tab = []
ecrete(tab, 0, 10)
assert tab == []
